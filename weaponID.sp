#include <sourcemod>
#include <cstrike>
#include <smlib>

public Plugin:myinfo = 
{
	name = "Weapon ID",
	author = "korqee"
}

public OnPluginStart()
{
	RegConsoleCmd("sm_weaponid", Command_WeaponID);
}

public Action:Command_WeaponID(client, args)
{
	if(!args)
	{
		new CSWeaponID:CSWeapon;
		new iNewWeapon = Client_GetActiveWeapon(client);
		if (IsValidEntity(iNewWeapon) && IsValidEdict(iNewWeapon))
		{
			new String:sClassName[64];
			GetEdictClassname(iNewWeapon, sClassName, 64);
			ReplaceString(sClassName, 64, "weapon_", "", false);
			new String:sWeaponAlias[64];
			CS_GetTranslatedWeaponAlias(sClassName, sWeaponAlias, 64);
			new CSWeaponID:weaponID = CS_AliasToWeaponID(sWeaponAlias);
			CSWeapon = weaponID;
			
			PrintToChat(client, "\n\n\nCSWeapon = %d", CSWeapon);
			PrintToChat(client, "iNewWeapon = %d", iNewWeapon);
			PrintToChat(client, "sClassName = %s", sClassName);
			PrintToChat(client, "sWeaponAlias = %s", sWeaponAlias);
			PrintToChat(client, "weaponID = %d", weaponID);
		}
		else
		{
			PrintToChat(client, "Invalid entity");
		}
	}
	else
	{
		new String:sWeaponID[32];
		GetCmdArgString(sWeaponID, 32);
		
		new CSWeaponID:WeaponID = StringToInt(sWeaponID);

		decl String:sAlias[64];
		CS_WeaponIDToAlias(WeaponID, sAlias, 64);
		PrintToChat(client, "sAlias = weapon_%s", sAlias);
	}
	
	return Plugin_Handled;
}