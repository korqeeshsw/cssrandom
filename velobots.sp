#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks> 
#include <smlib>

#define MAX_TYPES 2

new Handle:g_hGhostStartPauseTime,
	Handle:g_hGhostEndPauseTime;
	
/* -- Map -- */
new gI_ReplayTick[MAX_TYPES],
	gI_ReplayBotClient[MAX_TYPES];
new Handle:gA_Frames[MAX_TYPES];

new	String:gS_BotTime[MAX_TYPES][32],			//Run's time
	String:gS_BotLastName[MAX_TYPES][32],		//Last player's name
	String:gS_BotSteamID[MAX_TYPES][32];		//Player's SteamID
	
new bool:g_GhostPaused[MAX_TYPES];
new Float:g_fPauseTime[MAX_TYPES];

//Client
new Handle:gA_PlayerFrames[MAXPLAYERS+1];
new	Float:EndTime[MAXPLAYERS+1];

new String:gS_Map[128];

new bool:gB_Recording[MAXPLAYERS+1] = {false, ...};
new bool:gB_Paused[MAXPLAYERS+1] = {false, ...};
new bool:ThrowedFlash[MAXPLAYERS + 1] =  { true, ... };
new bool:g_Debug = false;

//SQL
new Handle:g_hSQL = INVALID_HANDLE; 

public Plugin:myinfo = 
{
	name = "Replay bots",
	author = "korqee",
	description = "Bot replay for trikz",
	version = "1.0",
	url = "+79124197803"
}


/*
"kq_bot" 
{ 
	"driver"    	"sqlite" 
	"host"    		"localhost" 
	"database"    	"kq_bot" 
	"user"    		"root" 
	"pass"    		"" 
}
*/

public OnPluginStart()
{
	if(!SQL_CheckConfig("kq_bot")) 
	{ 
		SetFailState("[SQLite] 'kq_bot' wasn't found in databases.cfg"); 
		return; 
	} 

	new String:error[128];
	g_hSQL = SQLite_UseDatabase("kq_bot", error, 256);
	
	SQL_TQuery(g_hSQL, SQL_DatabaseCreateCallback, "CREATE TABLE IF NOT EXISTS `players` (`steamid` varchar(32) NOT NULL, `lastname` varchar(32) NOT NULL, PRIMARY KEY (`steamid`))"); 

	g_hGhostStartPauseTime = CreateConVar("timer_ghoststartpause", "2.0", "How long the ghost will pause before starting its run.");
	g_hGhostEndPauseTime   = CreateConVar("timer_ghostendpause", "2.0", "How long the ghost will pause after it finishes its run.");

	CreateTimer(5.0, BotCheck, INVALID_HANDLE, TIMER_REPEAT);
	
	RegConsoleCmd("sm_replay_debug", Cmd_DebugMode);
	RegConsoleCmd("sm_record.start", Command_StartRecord);
	RegConsoleCmd("sm_record.stop", Command_EndRecord);
	RegConsoleCmd("sm_record.pause", Command_Pause);
	RegConsoleCmd("sm_record.unpause", Command_UnPause);
	
	for(new i = 1; i <= MaxClients; i++)
	{
		OnClientPutInServer(i);
	}
}

public SQL_DatabaseCreateCallback(Handle:owner, Handle:hndl, const String:error[], any:data) 
{ 
	if (hndl == INVALID_HANDLE) LogError(error); 
}

stock bool:IsValidPlayer(client)
{
    if (client < 1 || client > MaxClients || !IsClientConnected(client))
        return false;
   
    return true;
}

public OnPluginEnd()
{	
	ServerCommand("sm_cvar bot_quota 0");
}

public OnClientPostAdminCheck(client)
{
	new String:sAuth[32];
	GetClientAuthString(client, sAuth, 32);
	
	new String:sName[32];
	GetClientName(client, sName, 32);	
	
	decl String:query[512];
	FormatEx(query, sizeof(query), "INSERT OR REPLACE INTO players (steamid, lastname) VALUES ('%s', '%s');", sAuth, sName);
	
	SQL_TQuery(g_hSQL, SQL_ConnectCallback, query);
	
	for(new Type = 0; Type < MAX_TYPES; Type++) 
		LoadName(gS_BotSteamID[Type], Type);
}

public SQL_ConnectCallback(Handle:owner, Handle:hndl, const String:error[], any:data) 
{ 
	if (hndl == INVALID_HANDLE) LogError(error); 
}

// Input Example:
// sm_record.start <userid>
// sm_record.start 13
// It means player (13) is starting recording
//Also, if you want to test it from client type "sm_rcon " before
public Action:Command_StartRecord(client, args)
{
	decl String:arg[32];
	GetCmdArg(1, arg, sizeof(arg));

	if (IsValidPlayer(client))
	{
		
		gB_Recording[client] = true;
		gB_Paused[client] = false;
		
		StartRecord(client);		
		PrintToChatAll("\x03[Replay] %N (%d) started recording", client, client);	
		
	}
	else
	{
		PrintToChatAll("\x03[Replay] %N not valid", client);	
	}
		
	return Plugin_Stop;
}

// Input Example:
// sm_record.stop <userid> <Type> <Time> <validwr>
// sm_record.stop 14 0 45.67 1
// it means player (13) who was booster (0) stopped recording with time 14.00212 which had WR (1)
public Action:Command_EndRecord(client, args)
{
	if(args == 4)
	{
		new String:sBuffer[4][32];
		GetCmdArg(1, sBuffer[0], 32);
		GetCmdArg(2, sBuffer[1], 32);
		GetCmdArg(3, sBuffer[2], 32);
		GetCmdArg(4, sBuffer[3], 32);		
		
		new iTarget = StringToInt(sBuffer[0]);
		if (iTarget != 14)
		{
			return Plugin_Continue;
		}
		
		new Type = StringToInt(sBuffer[1]);
		new Float:Time = StringToFloat(sBuffer[2]);
		new bool:IsValidWR = StringToInt(sBuffer[3]);
		
		PrintToChatAll("\x03[Replay] %N ended recording", client);
	
		gB_Recording[client] = false;
		
		//EndRecord(iTarget, Type, Float:Time, bool:IsValidWR)
		EndRecord(client, Type, Float:Time, bool:IsValidWR)
	}
	
	return Plugin_Stop;
}

public Action:Command_Pause(client, args)
{
	new String:sBuffer[8];
	GetCmdArg(1, sBuffer, 8);
	new iTarget = StringToInt(sBuffer);
	
	PrintToChatAll("\x03[Replay] %N paused recording", iTarget);
	
	gB_Paused[iTarget] = true;
	
	return Plugin_Stop;
}

public Action:Command_UnPause(client, args)
{
	new String:sBuffer[8];
	GetCmdArg(1, sBuffer, 8);
	new iTarget = StringToInt(sBuffer);
	
	PrintToChatAll("\x03[Replay] %N unpaused recording", iTarget);
	
	gB_Paused[iTarget] = false;
	
	return Plugin_Stop;
}

public OnEntityCreated(entity, const String:classname[])
{
	if (StrEqual(classname, "func_button", true))
	{
		SDKHook(entity, SDKHook_Use, OnTrigger);
	}
	else
	{
		if (StrContains(classname, "trigger_", true) != -1)
		{
			SDKHook(entity, SDKHook_StartTouch, OnTrigger);
			SDKHook(entity, SDKHook_EndTouch, OnTrigger);
			SDKHook(entity, SDKHook_Touch, OnTrigger);
		}
	}
}

public Action:OnTrigger(entity, other)
{
	if(0 < other <= MaxClients)
	{
		if(IsClientConnected(other))
		{
			if(IsFakeClient(other))
			{
				return Plugin_Handled;
			}
		}
	}
   
	return Plugin_Continue;
}

public Action:Cmd_DebugMode(client, args)
{
	g_Debug = !g_Debug;
	if(g_Debug) PrintToChat(client, "DEBUG-Replay Mode enabled!");
	else PrintToChat(client, "DEBUG-Replay Mode disabled!");
}

public Action:BotCheck(Handle:Timer)
{
	new String:sTempFullName[MAX_TYPES][32];
	
	gI_ReplayTick[0] = gI_ReplayTick[1];
	
	/* -- Map -- */
	for(new Type = 0; Type < MAX_TYPES; Type++) 
	{
		LoadName(gS_BotSteamID[Type], Type);
		
		if(!StrEqual(gS_BotTime[Type], ""))
		{
			FormatEx(sTempFullName[Type], MAX_NAME_LENGTH, "%s • %s", gS_BotTime[Type], gS_BotLastName[Type]);
		}
		
		if(gI_ReplayBotClient[Type] > 0)
		{
			if(!IsPlayerAlive(gI_ReplayBotClient[Type]))
			{
				CS_RespawnPlayer(gI_ReplayBotClient[Type]);
			}
			
			SetClientInfo(gI_ReplayBotClient[Type], "name", sTempFullName[Type]);
			
			SetEntProp(gI_ReplayBotClient[Type], Prop_Data, "m_takedamage", 0, 1);
			
			//SetEntProp(gI_ReplayBotClient[Type], Prop_Send, "m_nSolidType", 0);	//invisible4triggers
			SetEntProp(gI_ReplayBotClient[Type], Prop_Data, "m_nSolidType", 0);
		}
	}
}

public OnClientPutInServer(client)
{
	gA_PlayerFrames[client] = CreateArray(8);
}

public OnMapStart()
{
	/* -- Map -- */
	for(new Type = 0; Type < MAX_TYPES; Type++) 
	{
		gI_ReplayTick[Type] = 0;
		gI_ReplayBotClient[Type] = 0;
		FormatEx(gS_BotTime[Type], MAX_NAME_LENGTH, "");
		FormatEx(gS_BotLastName[Type], MAX_NAME_LENGTH, "Loading...");
		FormatEx(gS_BotSteamID[Type], MAX_NAME_LENGTH, "No SteamID");		
	}
	
	GetCurrentMap(gS_Map, 128);
	RemoveMapPath(gS_Map, gS_Map, 128);
	
	new String:sTempMap[140];
	Format(sTempMap, 140, "maps/%s.nav", gS_Map);
	
	if(!FileExists(sTempMap))
	{
		File_Copy("maps/base.nav", sTempMap);
		
		ForceChangeLevel(gS_Map, ".nav file generate");
		
		return;
	}
	
	ServerCommand("bot_kick");
	
	new bot_quota = 0;
	
	for(new i = 0; i < MAX_TYPES; i++) 
		bot_quota++;
	
	for(new n = 0; n < bot_quota; n++)
		gI_ReplayTick[n] = 0;
	
	ServerCommand("sm_cvar bot_quota %d", bot_quota);

	new Handle:bot_join_after_player = FindConVar("bot_join_after_player");
	SetConVarString(bot_join_after_player, "0");
	
	new Handle:bot_chatter = FindConVar("bot_chatter");
	SetConVarString(bot_chatter, "off");	
	
	CreateTimer(10.0, LoadTimerReplays);

	CreateTimer(0.1, HUDTimer_CSS, _, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
}

RestartBots()
{
	for(new i = 0; i < MAX_TYPES; i++) 
	{
		gI_ReplayTick[i] = 0;
	}
}

public Action:HUDTimer_CSS(Handle:timer)
{
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsClientInGame(client) && !IsFakeClient(client))
			Update_BotHUDS(client);
	}

	return Plugin_Continue;
}

Update_BotHUDS(client)
{
	new iClientToShow, iObserverMode;

	// Show own buttons by default
	iClientToShow = client;
	
	// Get target he's spectating
	if(!IsPlayerAlive(client) || IsClientObserver(client))
	{
		iObserverMode = GetEntProp(client, Prop_Send, "m_iObserverMode");
		
		//if(iObserverMode == SPECMODE_FIRSTPERSON || iObserverMode == SPECMODE_3RDPERSON)
		if(iObserverMode == 4 || iObserverMode == 5)
		{
			iClientToShow = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
			
			// Check client index
			if(iClientToShow <= 0 || iClientToShow > MaxClients)
				return; 
		}
		else
		{
			return; // don't proceed, if in freelook..
		}
	}
	
	if(IsFakeClient(iClientToShow))
	{
		new Float:fVelocity[3];
		GetEntPropVector(iClientToShow, Prop_Data, "m_vecVelocity", fVelocity);	
		new Float:currentspeed = SquareRoot(Pow(fVelocity[0],2.0)+Pow(fVelocity[1],2.0)); //player speed (units per secound)XY
		
		
		new iSize[2];
		new Float:fPTS[2];
		new String:sTime[2][32];
		new Float:fTempTime[2];
		
		//FormatSeconds(Float:time, String:newtime[], newtimesize)
		
		/* -- Map -- */
		for(new i = 0; i < MAX_TYPES; i++) 
		{
			if(gA_Frames[i] != INVALID_HANDLE)
				iSize[i] = GetArraySize(gA_Frames[i]) - 3;	
				
			fPTS[i] = float(gI_ReplayTick[i]) / float(iSize[i]) * 100.0;
			
			fTempTime[i] = float(gI_ReplayTick[i]) / 100.0;
			
			FormatSeconds(fTempTime[i], sTime[i], 32);
			
			if(fPTS[i] > 100.0) fPTS[i] = 100.0;
			else if(fPTS[i] < 0.0) fPTS[i] = 0.0;
			
			if(iClientToShow == gI_ReplayBotClient[i])
				PrintHintText(client, "%s\n-\n( %s )\n[ %.2f\%%%% ]\n\nSpeed: %d", gS_BotLastName[i], sTime[i], fPTS[i], RoundToFloor(currentspeed));
		}
	}
}

public OnMapEnd()
{
	// Remove ghost to get a clean start next map
	ServerCommand("bot_kick all");
	
	/* -- Map -- */
	for(new i = 0; i < MAX_TYPES; i++) 
	{
		gI_ReplayTick[i] = 0;
		gI_ReplayBotClient[i] = 0;
		FormatEx(gS_BotTime[i], MAX_NAME_LENGTH, "");
		FormatEx(gS_BotLastName[i], MAX_NAME_LENGTH, "Loading...");
		FormatEx(gS_BotSteamID[i], MAX_NAME_LENGTH, "No SteamID");		
	}
}

public Action:LoadTimerReplays(Handle:Timer)
{
	new String:sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, PLATFORM_MAX_PATH, "data/trikzreplay");
	
	if(!DirExists(sPath))
	{
		CreateDirectory(sPath, 511);
	}

	/* -- Map -- */
	for(new i = 0; i < MAX_TYPES; i++) 
	{
		gA_Frames[i] = CreateArray(8);
		LoadReplay(i);		
	}
}

public bool:LoadReplay(Type)
{
	new String:sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, PLATFORM_MAX_PATH, "data/trikzreplay/%s_%d.rec", gS_Map, Type);

	if(FileExists(sPath))
	{
		new Handle:hFile = OpenFile(sPath, "r");
		
		ReadFileLine(hFile, gS_BotTime[Type], 32);
		ReadFileLine(hFile, gS_BotSteamID[Type], 32);
		
		TrimString(gS_BotTime[Type]);
		TrimString(gS_BotSteamID[Type]);
		
		new String:sLine[320];
		new String:sExplodedLine[8][64];
		
		ReadFileLine(hFile, sLine, 320);
		
		new iSize = 0;
		
		while(!IsEndOfFile(hFile))
		{
			ReadFileLine(hFile, sLine, 320);
			ExplodeString(sLine, "|", sExplodedLine, 8, 64);
		
			iSize = GetArraySize(gA_Frames[Type]) + 1;
			ResizeArray(gA_Frames[Type], iSize);

			SetArrayCell(gA_Frames[Type], iSize - 1, StringToFloat(sExplodedLine[0]), 0);
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToFloat(sExplodedLine[1]), 1);
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToFloat(sExplodedLine[2]), 2);			
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToFloat(sExplodedLine[3]), 3);
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToFloat(sExplodedLine[4]), 4);
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToInt(sExplodedLine[5]), 5);
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToInt(sExplodedLine[6]), 6);
			SetArrayCell(gA_Frames[Type], iSize - 1, StringToInt(sExplodedLine[7]), 7);
		}
		
		CloseHandle(hFile);
	}
	
	return true;
}

public bool:OnClientConnect(client, String:rejectmsg[], maxlen)
{
	if(IsFakeClient(client))
	{
		for(new i = 0; i < MAX_TYPES; i++)
		{
			if(gI_ReplayBotClient[i] == 0)
			{
				gI_ReplayBotClient[i] = client;
				
				new String:sBotTag[32] = "N REPLAY";
				
				CS_SetClientClanTag(gI_ReplayBotClient[i], sBotTag);
				SetClientInfo(gI_ReplayBotClient[i], "name", "No Record");
				break;
			}
		}
	}
	
	return true;
}

public OnClientDisconnect(client)
{
	/* -- Map -- */
	for(new i = 0; i < MAX_TYPES; i++) 
	{
		if(client == gI_ReplayBotClient[i])
		{
			gI_ReplayBotClient[i] = 0;
		}
	}
}

public SaveReplay(client, Type)
{
	new String:sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, PLATFORM_MAX_PATH, "data/trikzreplay/%s_%d.rec", gS_Map, Type);
	
	if(DirExists(sPath))
	{
		DeleteFile(sPath);
	}
	
	new Handle:hFile = OpenFile(sPath, "w");
	
	WriteFileLine(hFile, gS_BotTime[Type]);
	WriteFileLine(hFile, gS_BotSteamID[Type]);
	
	new iSize = GetArraySize(gA_Frames[Type]);

	new String:sBuffer[512];
	
	for(new i = 0; i < iSize; i++)
	{
		FormatEx(sBuffer, sizeof(sBuffer), "%f|%f|%f|%f|%f|%d|%d|%d", GetArrayCell(gA_Frames[Type], i, 0),
																	GetArrayCell(gA_Frames[Type], i, 1),
																	GetArrayCell(gA_Frames[Type], i, 2),
																	GetArrayCell(gA_Frames[Type], i, 3),
																	GetArrayCell(gA_Frames[Type], i, 4),
																	GetArrayCell(gA_Frames[Type], i, 5),
																	GetArrayCell(gA_Frames[Type], i, 6),
																	GetArrayCell(gA_Frames[Type], i, 7));
		
		WriteFileLine(hFile, sBuffer);
	}
	
	CloseHandle(hFile);
}


public StartRecord(client)
{
	if(!IsFakeClient(client))
	{
		ClearArray(gA_PlayerFrames[client]);
	}
}

public EndRecord(client, Type, Float:Time, bool:IsValidWR)
{
	if(IsValidWR)
	{
		EndTime[client] = GetEngineTime() + 3;
		
		GetClientName(client, gS_BotLastName[Type], 32);
		FormatSeconds(Time, gS_BotTime[Type], 32);
		GetClientAuthString(client, gS_BotSteamID[Type], 32);
		
		new String:sTempFullName[32];
		FormatEx(sTempFullName, 32, "%s • %s", gS_BotTime[Type], gS_BotLastName[Type]);
		
		SetClientInfo(gI_ReplayBotClient[Type], "name", sTempFullName);
		
		gI_ReplayTick[Type] = 0;

		gA_Frames[Type] = CloneArray(gA_PlayerFrames[client]);
		
		ClearArray(gA_PlayerFrames[client]);
		SaveReplay(client, Type);
	}
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if(IsPlayerAlive(client))
	{
		if(!IsFakeClient(client))
		{
			if(gB_Recording[client] && !gB_Paused[client])
			{
				// Weapons part
				new CSWeaponID:CSWeapon;
				new iNewWeapon = Client_GetActiveWeapon(client);
				if (IsValidEntity(iNewWeapon) && IsValidEdict(iNewWeapon))
				{
					new String:sClassName[64];
					GetEdictClassname(iNewWeapon, sClassName, 64);
					ReplaceString(sClassName, 64, "weapon_", "");
					new String:sWeaponAlias[64];
					CS_GetTranslatedWeaponAlias(sClassName, sWeaponAlias, 64);
					new CSWeaponID:weaponId = CS_AliasToWeaponID(sWeaponAlias);
					CSWeapon = weaponId;
				}
				
				// Record player movement data
				new iSize = GetArraySize(gA_PlayerFrames[client]);
				ResizeArray(gA_PlayerFrames[client], iSize + 1);
				
				new Float:vPos[3], Float:vAng[3];
				GetEntPropVector(client, PropType:1, "m_vecOrigin", vPos, 0);
				GetClientEyeAngles(client, vAng);
				
				SetArrayCell(gA_PlayerFrames[client], iSize, vPos[0], 0);
				SetArrayCell(gA_PlayerFrames[client], iSize, vPos[1], 1);
				SetArrayCell(gA_PlayerFrames[client], iSize, vPos[2], 2);
				SetArrayCell(gA_PlayerFrames[client], iSize, vAng[0], 3);
				SetArrayCell(gA_PlayerFrames[client], iSize, vAng[1], 4);
				SetArrayCell(gA_PlayerFrames[client], iSize, buttons, 5);
				SetArrayCell(gA_PlayerFrames[client], iSize, impulse, 6);
				SetArrayCell(gA_PlayerFrames[client], iSize, CSWeapon, 7);
			}
		}
		else
		{
			/* -- Map -- */
			for(new Type = 0; Type < MAX_TYPES; Type++) 
			{					
				if(client == gI_ReplayBotClient[Type] && gA_Frames[Type] != INVALID_HANDLE)
				{
					new iSize = GetArraySize(gA_Frames[Type]);					
					new Float:vPos[3], Float:vAng[3];
					
					//g_fStartReplayTime[Type] = g_fPauseTime[Type] + GetConVarFloat(g_hGhostStartPauseTime);
					
					if(gI_ReplayTick[Type] == 0)
					{					
						if(iSize > 0)
						{
							vPos[0] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 0);
							vPos[1] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 1);
							vPos[2] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 2);
							vAng[0] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 3);
							vAng[1] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 4);
							buttons = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 5);
							impulse = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 6);
							Client_RemoveAllWeapons(gI_ReplayBotClient[Type], "", false);
							
							TeleportEntity(gI_ReplayBotClient[Type], vPos, vAng, Float:{0.0, 0.0, 0.0});
						}
						
						if(g_GhostPaused[Type] == false)
						{
							g_GhostPaused[Type] = true;
							g_fPauseTime[Type]  = GetEngineTime();
						}
						
						if(GetEngineTime() > g_fPauseTime[Type] + GetConVarFloat(g_hGhostStartPauseTime))
						{
							g_GhostPaused[Type] = false;
							gI_ReplayTick[Type]++;
						}
					}
					else if(gI_ReplayTick[Type] == (iSize - 1))
					{
						if(iSize > 0)
						{
							vPos[0] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 0);
							vPos[1] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 1);
							vPos[2] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 2);
							vAng[0] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 3);
							vAng[1] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 4);
							buttons = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 5);
							impulse = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 6);
							
							TeleportEntity(gI_ReplayBotClient[Type], vPos, vAng, Float:{0.0, 0.0, 0.0});
						}
						
						if(g_GhostPaused[Type] == false)
						{					
							g_GhostPaused[Type] = true;
							g_fPauseTime[Type]  = GetEngineTime();
						}
						
						if(GetEngineTime() > g_fPauseTime[Type] + GetConVarFloat(g_hGhostEndPauseTime))
						{
							g_GhostPaused[Type] = false;
							RestartBots();
						}
					}
					else if(gI_ReplayTick[Type] < iSize)
					{
						new Float:vPos2[3];
						GetEntPropVector(gI_ReplayBotClient[Type], PropType:1, "m_vecOrigin", vPos2, 0);
						
						vPos[0] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 0);
						vPos[1] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 1);
						vPos[2] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 2);
						vAng[0] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 3);
						vAng[1] = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 4);
						buttons = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 5);
						impulse = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 6);

						
						if(GetVectorDistance(vPos, vPos2) > 50.0)
						{
							TeleportEntity(gI_ReplayBotClient[Type], vPos, vAng, NULL_VECTOR);
						}
						else
						{
							// Get the new velocity from the the 2 points
							new Float:vVel[3];
							MakeVectorFromPoints(vPos2, vPos, vVel);
							ScaleVector(vVel, 100.0);
							
							TeleportEntity(gI_ReplayBotClient[Type], NULL_VECTOR, vAng, vVel);
						}
						
						if(GetEntityFlags(gI_ReplayBotClient[Type]) & FL_ONGROUND)
							SetEntityMoveType(gI_ReplayBotClient[Type], MOVETYPE_WALK);
						else
							SetEntityMoveType(gI_ReplayBotClient[Type], MOVETYPE_NOCLIP);
						
						gI_ReplayTick[Type] = (gI_ReplayTick[Type] + 1) % iSize;
						
						new CSWeaponID:newWeapon = GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 7);
						if (newWeapon)
						{
							new iCurrentWeapon = Client_GetActiveWeapon(client);
							new CSWeaponID:CurrentweaponId;
							if (IsValidEntity(iCurrentWeapon) && IsValidEdict(iCurrentWeapon))
							{
								new String:sClassName[64];
								GetEdictClassname(iCurrentWeapon, sClassName, 64);
								ReplaceString(sClassName, 64, "weapon_", "");
								new String:sWeaponAlias[64];
								CS_GetTranslatedWeaponAlias(sClassName, sWeaponAlias, 64);
								CurrentweaponId = CS_AliasToWeaponID(sWeaponAlias);
								if((GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type], 5) & IN_ATTACK) 
									&& !(GetArrayCell(gA_Frames[Type], gI_ReplayTick[Type] + 1, 5) & IN_ATTACK)
									&& StrEqual("flashbang", sClassName, false) 
									&& ThrowedFlash[client])
								{
									ThrowFlash(client);
								}
							}
							if (CurrentweaponId != newWeapon)
							{
								Client_RemoveAllWeapons(client, "", false);
								
								decl String:sAlias[64];
								CS_WeaponIDToAlias(newWeapon, sAlias, 64);
								Format(sAlias, 64, "weapon_%s", sAlias);
								GivePlayerItem(client, sAlias, 0);
							}
						}
						else
						{
							Client_RemoveAllWeapons(client, "", false);
						}
						
						if(g_GhostPaused[Type] == true)
						{
							if(GetEntityMoveType(gI_ReplayBotClient[Type]) != MOVETYPE_NONE)
							{
								SetEntityMoveType(gI_ReplayBotClient[Type], MOVETYPE_NONE);
							}
						}
					}
					//gI_ReplayTick[Type] = gI_HUDTick[Type];
				}
			}
			/* -- End Map -- */
		}
	}
	
	return Plugin_Changed;
}

ThrowFlash(client)
{
	//PrintToChatAll("ThrowedFlash[client] = false");
	
	ThrowedFlash[client] = false;

	CreateTimer(0.2, ExpireCoolDown, client);
}

public Action:ExpireCoolDown(Handle:Timer, any:client)
{
	//PrintToChatAll("HERE!!!");
	Client_RemoveAllWeapons(client, "", false);
	GivePlayerItem(client, "weapon_flashbang", 0);
	
	ThrowedFlash[client] = true;
}

stock SubString(const String:source[], start, len, String:destination[], maxlen)
{
	if(maxlen < 1)
	{
		ThrowError("Destination size must be 1 or greater, but was %d", maxlen);
	}
	
	if(len == 0)
	{
		destination[0] = '\0';
		
		return true;
	}
	
	if(start < 0)
	{
		start = strlen(source) + start;
		
		if(start < 0)
		{
			start = 0;
		}
	}
	
	if(len < 0)
	{
		len = strlen(source) + len - start;
		
		if(len < 0)
		{
			return false;
		}
	}
	
	new realLength = len + 1 < maxlen? len + 1:maxlen;
	
	strcopy(destination, realLength, source[start]);
	
	return true;
}

stock RemoveMapPath(const String:map[], String:destination[], maxlen)
{
	if(strlen(map) < 1)
	{
		ThrowError("Bad map name: %s", map);
	}
	
	new pos = FindCharInString(map, '/', true);
	
	if(pos == -1)
	{
		pos = FindCharInString(map, '\\', true);
		
		if(pos == -1)
		{
			strcopy(destination, maxlen, map);
			return false;
		}
	}

	new len = strlen(map) - 1 - pos;
	
	SubString(map, pos + 1, len, destination, maxlen);
	
	return true;
}

FormatSeconds(Float:time, String:newtime[], newtimesize)
{
	new iTemp = RoundToFloor(time);
	
	new iHours;
	new iMinutes;

	if(iTemp > 3600)
	{
		iHours = RoundToFloor(iTemp / 3600.0);
		iTemp %= 3600;
	}

	new String:sHours[8];

	if(iHours < 10)
	{
		FormatEx(sHours, 8, "0%d", iHours);
	}
	else
	{
		FormatEx(sHours, 8, "%d", iHours);
	}
	
	if(iTemp >= 60)
	{
		iMinutes = RoundToFloor(iTemp / 60.0);
		iTemp %= 60;
	}

	new String:sMinutes[8];

	if(iMinutes < 10)
	{
		FormatEx(sMinutes, 8, "0%d", iMinutes);
	}
	else
	{
		FormatEx(sMinutes, 8, "%d", iMinutes);
	}
	
	new Float:fSeconds = ((iTemp) + time - RoundToFloor(time));

	new String:sSeconds[16];

	if(fSeconds < 10)
	{
		FormatEx(sSeconds, 16, "0%.02f", fSeconds);
	}
	else
	{
		FormatEx(sSeconds, 16, "%.02f", fSeconds);
	}

	if(iHours > 0)
	{
		FormatEx(newtime, newtimesize, "%s:%s:%s", sHours, sMinutes, sSeconds);
	}
	else if(iMinutes > 0)
	{
		FormatEx(newtime, newtimesize, "%s:%s", sMinutes, sSeconds);
	}
	else
	{
		FormatEx(newtime, newtimesize, "00:%.02f", fSeconds);
	}
}

LoadName(String:Auth[], Type)
{	
	if (g_hSQL != INVALID_HANDLE)
	{
		decl String:query[128];
		FormatEx(query, sizeof(query), "SELECT `lastname` FROM players WHERE `steamid` = '%s'", Auth);
		SQL_TQuery(g_hSQL, LoadNameCallback, query, Type, DBPrio_Normal);
	}
}

public LoadNameCallback(Handle:owner, Handle:hndl, const String:error[], any:Type)
{
	if (hndl == INVALID_HANDLE) 
	{
		LogError(error); 
	}
	
	while (SQL_FetchRow(hndl))
	{
		SQL_FetchString(hndl, 0, gS_BotLastName[Type], 32);
	}
}